﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using projectworkflows.application.Interfaces;
using projectworkflows.application.Models;
using projectworkflows.application.Processes;
using projectworkflows.tests.Fakes;

namespace projectworkflows.tests
{
    [TestFixture]
    public class WorkflowTests
    {
        [Test]
        public void Workflow_Works()
        {
            // Arrange
            // I have a workflow step
            var endStep = FakeStep.CreateStep("Final", true, null, null);
            var rootStep = FakeStep.CreateStep("First", true, endStep, null);

            IWorkflowProcess workflowProcess = new WorkflowProcess(rootStep);

            // Act
            var result = workflowProcess.Process();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.IsInstanceOf<WorkflowProcessResult>(result);
            Assert.That(result.TotalSteps, Is.EqualTo(2));
            Assert.That(result.ResponseFlow, Is.Not.Null);
            Assert.IsInstanceOf<IList<StepResult>>(result.ResponseFlow);
            Assert.That(result.ResponseFlow.Count, Is.EqualTo(2));

            var firstResponse = result.ResponseFlow.ToArray()[0];
            AssertResponse(firstResponse, "First", true);

            var secondResponse = result.ResponseFlow.ToArray()[1];
        }

        private void AssertResponse(StepResult response, string expectedName, bool expectedResult)
        {
           Assert.That(response, Is.Not.Null);
           Assert.That(response.Name, Is.EqualTo(expectedName));
           Assert.That(response.ProcessResult, Is.EqualTo(expectedResult));
        }
    }
}
