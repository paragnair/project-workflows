﻿using projectworkflows.application.Interfaces;
using System;

namespace projectworkflows.tests.Fakes
{
    public class FakeStepHasSuccessStep : IWorkflowStep
    {
        public string Name
        {
            get { return this.GetType().ToString(); }

        }
        public bool Process()
        {
            return true;
        }

        public IWorkflowStep SuccesStep { get; }
        public IWorkflowStep FailureStep { get; }
        public void Anyway()
        {
            throw new NotImplementedException();
        }
    }
}
