﻿using projectworkflows.application.Interfaces;

namespace projectworkflows.tests.Fakes
{
    public class FakeStep : IWorkflowStep
    {
        public bool ExpectedProcessResponse;
        public IWorkflowStep ExpectedSuccessStep;
        public IWorkflowStep ExpectedFailureStep;


        public FakeStep(string name)
        {
            this.Name = name;
        }

        public static FakeStep CreateStep(string name,
            bool processResponse,
            IWorkflowStep successStep,
            IWorkflowStep failureStep)
        {
            var step = new FakeStep(name);
            step.ExpectedSuccessStep = successStep;
            step.ExpectedFailureStep = failureStep;
            step.ExpectedProcessResponse = processResponse;
            return step;
        }

        // Interface implementation
        public string Name { get; }
        public bool Process()
        {
            return ExpectedProcessResponse;
        }

        public IWorkflowStep SuccesStep
        {
            get { return ExpectedSuccessStep; }

        }
        public IWorkflowStep FailureStep { get { return ExpectedFailureStep; } }
        public void Anyway()
        {
            // do nothing
        }
    }
}
