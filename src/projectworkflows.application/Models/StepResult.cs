﻿namespace projectworkflows.application.Models
{
    public class StepResult
    {
        public string Name { get; set; }

        public bool ProcessResult { get; set; }

        public string ErrorMessage { get; set; }

        public override string ToString()
        {
            var asString = $"{Name} : {ProcessResult}";

            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                asString += $" ({ErrorMessage})";
            }

            return asString;
        }

        public static StepResult CreateFailure(string name, string message)
        {
            return new StepResult()
            {
                Name = name,
                ProcessResult = false,
                ErrorMessage = message
            };
        }

        public static StepResult CreateSuccess(string name)
        {
            return new StepResult()
            {
                Name = name,
                ProcessResult = true
            };
        }
    }
}