﻿using projectworkflows.application.Interfaces;

namespace projectworkflows.application.Models
{
    public class FlowResult<TInput, TOutput>
        where TInput : Input
        where TOutput : Output
    {
        public bool Succeeded { get; set; }

        public IModule<TInput, TOutput> SuccessStep { get; set; }

        public IModule<TInput, TOutput> FailureStep { get; set; }

        public void Anyway() { }
    }
}