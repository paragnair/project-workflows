﻿using System.Collections.Generic;

namespace projectworkflows.application.Models
{
    public class WorkflowProcessResult
    {
        public IList<StepResult> ResponseFlow { get; set; }

        public int TotalSteps { get; set; }
    }
}
