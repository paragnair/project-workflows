﻿using projectworkflows.application.Interfaces;
using projectworkflows.application.Models;
using System;
using System.Collections.Generic;

namespace projectworkflows.application.Processes
{
    public class WorkflowProcess : IWorkflowProcess
    {
        private readonly IWorkflowStep _root;


        public WorkflowProcess(IWorkflowStep root)
        {
            _root = root;
        }

        public WorkflowProcessResult Process()
        {
            var output = new WorkflowProcessResult() { TotalSteps = 0, ResponseFlow = new List<StepResult>() };

            var currentStep = _root;
            while (currentStep != null)
            {
                var stepSucceeded = true;
                // increment the step
                output.TotalSteps++;

                // process the current step
                try
                {
                    var result = currentStep.Process();
                    if (result)
                    {
                        output.ResponseFlow.Add(StepResult.CreateSuccess(currentStep.Name));
                    }
                    else
                    {
                        output.ResponseFlow.Add(StepResult.CreateFailure(currentStep.Name, "TODO"));
                        stepSucceeded = false;
                    }
                }
                catch (Exception ex)
                {
                    output.ResponseFlow.Add(StepResult.CreateFailure(currentStep.Name, ex.Message));
                }
                finally
                {
                    // execute the anyway
                    currentStep.Anyway();

                    // pick the next step
                    currentStep = stepSucceeded ? currentStep.SuccesStep : currentStep.FailureStep;
                }
            }


            return output;
        }
    }
}