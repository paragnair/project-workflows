﻿using projectworkflows.application.Models;

namespace projectworkflows.application.Interfaces
{
    public interface IWorkflowProcess
    {
        WorkflowProcessResult Process();
    }
}