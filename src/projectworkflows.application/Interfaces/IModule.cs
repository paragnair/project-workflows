﻿using System.Collections.Generic;

namespace projectworkflows.application.Interfaces
{
    public interface IModule<in TInput, TOutput>
        where TInput : Input
        where TOutput : Output
    {
        List<TOutput> Process(TInput input);
    }
}