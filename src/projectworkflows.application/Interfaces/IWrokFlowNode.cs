﻿namespace projectworkflows.application.Interfaces
{
    public interface IWrokFlowNode<TInput, TOutput>
        where TInput : Input
        where TOutput : Output
    {
        IWrokFlowNode<TInput,TOutput> Process(TInput input);
    }
}