﻿namespace projectworkflows.application.Interfaces
{
    public interface IWorkflowStep
    {
        string Name { get; }

        bool Process();

        IWorkflowStep SuccesStep { get; }

        IWorkflowStep FailureStep { get; }

        void Anyway();
    }
}