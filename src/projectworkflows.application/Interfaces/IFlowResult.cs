﻿namespace projectworkflows.application.Interfaces
{
    public interface IFlowResult<TInput, TOutput>
        where TInput : Input
        where TOutput : Output
    {

    }
}